Feature: Check addition

  This scenarios checking that the addition works correctly


  Scenario: Add x to y
    Given x is set to 8
    And y is set to 0
    When the calculator adds x to y
    Then the result is 20


  Scenario Outline: Add x to y using examples table
    Given x is set to <x>
    And y is set to <y>
    When the calculator adds x to y
    Then the result is <result>

    Examples:
      | x | y | result |
      | 1 | 1 | 2      |
      | 2 | 2 | 4      |