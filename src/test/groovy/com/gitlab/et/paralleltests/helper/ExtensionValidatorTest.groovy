package com.gitlab.et.paralleltests.helper

import com.gitlab.et.paralleltests.ParallelTestExtension
import spock.lang.Specification
import spock.lang.Unroll

class ExtensionValidatorTest extends Specification {

    def "Check unset mandatory parameter cucumberCliClass returns error"() {
        given:
        ParallelTestExtension extension = [parallelExecutions: 1,
                                           cucumberCliClass  : '',
                                           properties        : '',
                                           glue              : 'test/glue',
                                           featureDir        : 'test/features'
        ]

        when:
        ExtensionValidator extensionValidator = new ExtensionValidator(extension)
        extensionValidator.checkMandatoryParametersArePresent()

        then:
        def ex = thrown(MissingPropertyException)
        ex.message.contains("cucumberCliClass")
    }


    def "Check unset mandatory parameter glue returns error"() {
        given:
        ParallelTestExtension extension = [parallelExecutions: 1,
                                           cucumberCliClass  : 'cucumber.api.cli.Main',
                                           properties        : '',
                                           featureDir        : 'test/features'
        ]

        when:
        ExtensionValidator extensionValidator = new ExtensionValidator(extension)
        extensionValidator.checkMandatoryParametersArePresent()

        then:
        def ex = thrown(MissingPropertyException)
        ex.message.contains("glue")
    }


    def "Check unset mandatory parameter featureDir returns error"() {
        given:
        ParallelTestExtension extension = [parallelExecutions: 1,
                                           cucumberCliClass  : 'cucumber.api.cli.Main',
                                           properties        : '',
                                           glue              : 'test/glue'
        ]

        when:
        ExtensionValidator extensionValidator = new ExtensionValidator(extension)
        extensionValidator.checkMandatoryParametersArePresent()

        then:
        def ex = thrown(MissingPropertyException)
        ex.message.contains("featureDir")
    }


    def "Check mandatory parameter parallelExecutions returns error if equals 0"() {
        given:
        ParallelTestExtension extension = [parallelExecutions: 0,
                                           cucumberCliClass  : 'cucumber.api.cli.Main',
                                           properties        : '',
                                           glue              : 'test/glue',
                                           featureDir        : 'test/features'
        ]

        when:
        ExtensionValidator extensionValidator = new ExtensionValidator(extension)
        extensionValidator.checkMandatoryParametersArePresent()

        then:
        def ex = thrown(MissingPropertyException)
        ex.message.contains("parallelExecutions")
    }


    @Unroll
    def "buffer is empty is #isEmpty if parameter for printing configuration is #printIt"() {
        given:
        def buffer = new ByteArrayOutputStream()
        System.out = new PrintStream(buffer)

        and:
        ParallelTestExtension extension = [printConfiguration: printIt,
                                           parallelExecutions: 1,
                                           cucumberCliClass  : 'cucumber.api.cli.Main',
                                           properties        : '',
                                           glue              : 'test/glue',
                                           featureDir        : 'test/features',
                                           cucumberPlugins   : [
                                                   "html:ReportFolderParallel/",
                                                   "json:ReportFolderParallel/cucumber.json"
                                           ]
        ]

        when:
        ExtensionValidator extensionValidator = new ExtensionValidator(extension)
        extensionValidator.printConfiguration()

        then:
        buffer.toString().isEmpty() != isEmpty


        where:
        printIt | isEmpty
        true    | true
        false   | false

    }


}
