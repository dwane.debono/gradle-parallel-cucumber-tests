package com.gitlab.et.paralleltests.helper.model

import groovy.transform.EqualsAndHashCode

import static com.gitlab.et.paralleltests.helper.model.Result.*

@EqualsAndHashCode
class Test {
    String name
    List<String> command = new ArrayList<>()
    Result result = NOT_RUN
    long executionTimeInMilliSeconds = 0
}
