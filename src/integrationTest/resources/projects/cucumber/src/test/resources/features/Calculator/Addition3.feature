Feature: Check Addition

  This scenarios checking that the addition works correctly


  Scenario: Add numbers to correct result 3
    Given x is set to 8
    And y is set to 0
    When the calculator adds x to y
    Then the result is 8
